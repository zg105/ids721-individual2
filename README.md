# Individual Project 2 

This project implements a simple REST API/web service in Rust, using Docker for deployment.

## Get Start

1. Create a new Rust project running `cargo new individual2`

2. Add the required dependencies to `Cargo.toml` file.

3. In `main.rs`, I implement the functionality to calculate square of a number.

4. Run `cargo run` to test the functionality \
![](pic/1.png)
![](pic/2.png)


## Docker Configuration
1. Write the Dockerfile. It runs `cargo build --release` to build the project with optimized code. Finally, it defines the default command to run when the container starts, using `cargo run`. 

2. run `sudo docker build -t individual2 .` to build docker image: \
![](pic/3.png)

## CI/CD Pipeline
1. Write the `.gitlab-ci.yml` file, every time we push a change it will trigger the CICD workflow. We can also see the CI/CD job passes on GitLab. \
![](pic/4.png)

## Demo Video
You can also check out the demo video: 
