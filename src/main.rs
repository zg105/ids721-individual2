use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use serde::Serialize;
use std::collections::HashMap;

#[derive(Serialize)]
struct SquareResponse {
    number: i32,
    square: i32,
}

#[get("/")]
async fn index() -> impl Responder {
    "You can use the endpoint /square?number=<yournumber> to calculate the square of a number."
}

#[get("/square")]
async fn square(web::Query(info): web::Query<HashMap<String, String>>) -> impl Responder {
    match info.get("number") {
        Some(num_str) => {
            match num_str.parse::<i32>() {
                Ok(number) => {
                    let square = number * number;
                    let response = SquareResponse { number, square };
                    HttpResponse::Ok().json(response)
                }
                Err(_) => HttpResponse::BadRequest().body("Invalid number provided"),
            }
        }
        None => HttpResponse::BadRequest().body("Missing number query parameter"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(square)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
